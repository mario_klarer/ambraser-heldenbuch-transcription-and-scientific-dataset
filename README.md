<h1>Ambraser Heldenbuch: Transkription und wissenschaftliches Datenset</h1>

<h2>Metadaten</h2>

<ul>
<li>
<b>Projekttitel:</b> Ambraser Heldenbuch: Transkription und wissenschaftliches Datenset
</li>

<li>
<b>Projektleiter:</b> Univ.-Prof. Dr. Mario Klarer, Universität Innsbruck (https://orcid.org/0000-0003-0712-9328)
</li>

<li>
<b>Fördergeber:</b> Österreichische Akademie der Wissenschaften (ÖAW)
</li>

<li>
<b>Förderprogramm:</b> go!digital 2.0
</li>

<li>
<b>Projektwebsite:</b> https://www.uibk.ac.at/projects/ahb/
</li>

<li>
<b>Lizenz:</b> CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)
</li>
</ul>
